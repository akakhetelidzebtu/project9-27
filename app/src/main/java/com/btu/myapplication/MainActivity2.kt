package com.btu.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        Log.d("lifecyclemethods", "onCreateActivity2")
    }

    override fun onStart() {
        super.onStart()
        Log.d("lifecyclemethods", "onStartActivity2")
    }

    override fun onResume() {
        super.onResume()
        Log.d("lifecyclemethods", "onResumeActivity2")
    }

    override fun onPause() {
        super.onPause()
        Log.d("lifecyclemethods", "onPauseActivity2")
    }

    override fun onStop() {
        super.onStop()
        Log.d("lifecyclemethods", "onStopActivity2")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("lifecyclemethods", "onDestroyActivity2")
    }
}